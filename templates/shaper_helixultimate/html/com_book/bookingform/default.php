<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Book
 * @author     vst <lvson1087@gmail.com>
 * @copyright  2019 @ Bizappco
 * @license    bản quyền mã nguồn mở GNU phiên bản 2
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\HTML\HTMLHelper;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Uri\Uri;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;

HTMLHelper::_('behavior.keepalive');
HTMLHelper::_('behavior.tooltip');
HTMLHelper::_('behavior.formvalidation');
HTMLHelper::_('formbehavior.chosen', 'select');

// Load admin language file
$lang = Factory::getLanguage();
$lang->load('com_book', JPATH_SITE);
$doc = Factory::getDocument();
$doc->addScript(Uri::base() . '/media/com_book/js/form.js');

$user    = Factory::getUser();
$canEdit = BookHelpersBook::canUserEdit($this->item, $user);


?>

<div class="container">
    <div class="col-sm-6 col-md-offset-3">
        <div id="message-box" style="display:none;" class="alert"></div>
        <?php if (!$canEdit) : ?>
            <h3>
                <?php throw new Exception(Text::_('COM_BOOK_ERROR_MESSAGE_NOT_AUTHORISED'), 403); ?>
            </h3>
        <?php else : ?>
            <form id="form-booking"
                  action="<?php echo Route::_('index.php?option=com_book&task=booking.save'); ?>"
                  method="post" class="form-validate form-horizontal" enctype="multipart/form-data">
                <!-- SmartWizard 1 html -->
                <div id="smartwizard">
                    <ul>
                        <li><a href="#step-1">1</a><span>Nhập SĐT</span></li>
                        <li><a href="#step-2">2</a><span>Chọn địa chỉ</span></li>
                        <li><a href="#step-3">3</a><span>Chọn ngày giờ</span></li>
                    </ul>
                   <div>
                       <div id="step-1">
                           <div class="form-group">
                               <input type="text" name="jform[phone]" id="jform_phone" value="" class="form-control required" placeholder="Điện thoại" required="required" aria-required="true">
                           </div>
                           <?php //echo $this->form->renderField('phone'); ?>
                       </div>
                       <div id="step-2">
                           <?php echo $this->form->renderField('address'); ?>
                       </div>
                       <div id="step-3">
                         <div class="row">
                             <div class="col-sm-6">
                                 <?php echo $this->form->renderField('booking_date'); ?>
                             </div>
                             <div class="col-sm-6">
                                 <?php echo $this->form->renderField('hours'); ?>
                             </div>
                         </div>
                          <div class="row">
                              <?php echo $this->form->renderField('note'); ?>
                          </div>
                           <?php if ($this->canSave): ?>
                           <div class="text-right">
                               <button type="submit" id="btn-submit-book" class="validate btn btn-success">
                                   <?php echo Text::_('JSUBMIT'); ?>
                               </button>
                           </div>
                           <?php endif; ?>
                       </div>
                   </div>
                </div>
                <input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />

                <input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />

                <input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />

                <input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />

                <input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />
                <input type="hidden" name="jform[status_id]" value="1" />
                <input type="hidden" name="option" value="com_book"/>
                <input type="hidden" name="task" value="bookingform.save"/>
                <?php echo HTMLHelper::_('form.token'); ?>
            </form>
        <?php endif; ?>
    </div>

</div>
<script type="text/javascript">
    $ = jQuery.noConflict();
    $(document).ready(function () {
        $('form').on('submit', function () {
           $('#btn-submit-book').attr('disabled', 'disabled');
        });
        // Smart Wizard events
        $("#smartwizard").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
            let mgsBox = $("#message-box");
            let phoneInput = $('#jform_phone');
            let addressInput = $('#jform_address');
            if (stepNumber === 0){
                let phone = phoneInput.val();
                if (!phone){
                    mgsBox.addClass('alert-danger').text('Vui lòng nhập số điện thoại').show();
                    phoneInput.focus();
                    return false;
                }else{
                    mgsBox.removeClass('alert-danger').text('').hide();
                    return  true;
                }
            }else if(stepNumber === 1){
                let address = addressInput.val();
                if (!address){
                    mgsBox.addClass('alert-danger').text('Vui lòng chọn địa chỉ').show();
                    addressInput.focus();
                    return false;
                }else {
                    mgsBox.removeClass('alert-danger').text('').hide();
                    return  true;
                }
            }
            // var res = confirm("Do you want to leave the step "+stepNumber+"?");
            // if(!res){
            //     $("#message-box").append(" <strong>leaveStep</strong> Cancelled");
            // }else{
            //     $("#message-box").append(" <strong>leaveStep</strong> Allowed");
            // }
            // return res;
        });
        // Smart Wizard
        $('#smartwizard').smartWizard({
            selected: 0,
            theme: 'circles',
            transitionEffect:'fade',
            showStepURLhash: false
        });
    });
</script>