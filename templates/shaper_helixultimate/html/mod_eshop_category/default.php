<?php
/**
 * @version        1.3.1
 * @package        Joomla
 * @subpackage    EShop
 * @author    Giang Dinh Truong
 * @copyright    Copyright (C) 2012 Ossolution Team
 * @license        GNU/GPL, see LICENSE.php
 */
// no direct access
defined('_JEXEC') or die();
$showNumberProducts = false;
$prefix = JURI::base().'san-pham/';
?>
<div class="eshop-category">
    <ul>
        <?php

        foreach ($categories as $category) {
            $alias = $category->category_alias;
            if ($showNumberProducts) {
                $numberProducts = ' (' . EshopHelper::getNumCategoryProducts($category->id, true) . ')';
            } else {
                $numberProducts = '';
            }
            ?>
            <li <?php echo $category->childCategories ? 'class="has-sub"' : '' ?>>
                <?php
                $active = $category->id == $parentCategoryId ? ' class="active"' : '';
                ?>
                <a class="level_one"
                   href="<?= $prefix.$alias.'.html' ?>"<?php echo $active; ?>><?php echo $category->category_name . $numberProducts; ?></a>
                <?php
                if ($showChildren && $category->childCategories) {
                    ?>
                    <span class="btn_sub_menu"></span>
                    <ul class="sub-cate">
                        <?php
                        foreach ($category->childCategories as $childCategory) {
                            $subAlias = $alias.'/'.$childCategory->category_alias;
                            if ($showNumberProducts) {
                                $numberProducts = ' (' . EshopHelper::getNumCategoryProducts($childCategory->id, true) . ')';
                            } else {
                                $numberProducts = '';
                            }
                            ?>
                            <li>
                                <?php
                                $active = $childCategory->id == $childCategoryId ? 'class="active"' : '';
                                ?>
                                <a href="<?= $prefix.$subAlias.'.html' ?>" <?php echo $active; ?>>
                                    - <?php echo $childCategory->category_name . $numberProducts; ?></a>

                                <?php
                                if ($showChildren && count($childCategory->childCategories)) {
                                    ?>
                                    <span class="btn_sub_menu"></span>
                                    <ul class="sub-sub-cate">
                                        <?php
                                        foreach ($childCategory->childCategories as $grandCategory) {
                                            $grandAlias = $subAlias.'/'.$grandCategory->category_alias;
                                            if ($showNumberProducts) {
                                                $numberProducts = ' (' . EshopHelper::getNumCategoryProducts($grandCategory->id, true) . ')';
                                            } else {
                                                $numberProducts = '';
                                            }
                                            ?>
                                            <li>
                                                <?php
                                                $active = $grandCategory->id == $childCategoryId ? 'class="active"' : '';
                                                ?>
                                                <a href="<?= $prefix.$grandAlias.'.html' ?>" <?php echo $active; ?>>
                                                    - <?php echo $grandCategory->category_name . $numberProducts; ?></a>


                                            </li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                    <?php
                                }
                                ?>

                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                    <?php
                }
                ?>
            </li>
            <?php
        }
        ?>
    </ul>
</div>
