<?php
if($_REQUEST['downloadapp'] == 1){
	// android store
	if (preg_match('#android#i', $_SERVER ['HTTP_USER_AGENT'])) {
	    header('Location: market://details?id=com.bizappco.tuoikia');
	    exit;
	}

	// ios
	if (preg_match('#(iPad|iPhone|iPod)#i', $_SERVER ['HTTP_USER_AGENT'])) {
	    header('Location: itms-apps://itunes.apple.com/app/id1487435801');
	    exit;
	}

	// for another os
	//echo $_SERVER ['HTTP_USER_AGENT'];
}else{
	/**
	 * @package    Joomla.Site
	 *
	 * @copyright  Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
	 * @license    GNU General Public License version 2 or later; see LICENSE.txt
	 */

	/**
	 * Define the application's minimum supported PHP version as a constant so it can be referenced within the application.
	 */
	error_reporting(E_ALL);
	ini_set("display_errors", 1);
	error_reporting(0);
	ini_set("display_errors", 0);
	define('JOOMLA_MINIMUM_PHP', '5.3.10');

	if (version_compare(PHP_VERSION, JOOMLA_MINIMUM_PHP, '<'))
	{
		die('Your host needs to use PHP ' . JOOMLA_MINIMUM_PHP . ' or higher to run this version of Joomla!');
	}

	// Saves the start time and memory usage.
	$startTime = microtime(1);
	$startMem  = memory_get_usage();

	/**
	 * Constant that is checked in included files to prevent direct access.
	 * define() is used in the installation folder rather than "const" to not error for PHP 5.2 and lower
	 */
	define('_JEXEC', 1);

	if (file_exists(__DIR__ . '/defines.php'))
	{
		include_once __DIR__ . '/defines.php';
	}

	if (!defined('_JDEFINES'))
	{
		define('JPATH_BASE', __DIR__);
		require_once JPATH_BASE . '/includes/defines.php';
	}

	require_once JPATH_BASE . '/includes/framework.php';

	// Set profiler start time and memory usage and mark afterLoad in the profiler.
	JDEBUG ? JProfiler::getInstance('Application')->setStart($startTime, $startMem)->mark('afterLoad') : null;

	// Instantiate the application.
	$app = JFactory::getApplication('site');

	// Execute the application.
	$app->execute();

}
