
-- dien thoai cua hang
UPDATE `prfwj_eshop_configs` SET `config_value` = '0338117818' WHERE `config_key` = 'telephone';

-- dia chi cua hang
UPDATE `prfwj_eshop_configs` SET `config_value` = 'B3-077 Chợ đầu mối Bình Điền' WHERE `config_key` = 'address';


UPDATE prfwj_modules
SET content = REPLACE(content, 'Sạp 38 Chợ đầu mối Bình Điền, Quận 7', 'B3-077 Chợ đầu mối Bình Điền')
WHERE module = 'mod_custom';

UPDATE prfwj_modules
SET content = REPLACE(content, '0901.329.921', '0338117818')
WHERE module = 'mod_custom';

UPDATE prfwj_modules
SET content = REPLACE(content, '0901329921', '0338117818')
WHERE module = 'mod_custom';



UPDATE `prfwj_contact_details`
SET `address` = 'B3-077 Chợ đầu mối Bình Điền',
`telephone` = '0338117818',
`mobile` = '0338117818',
`email_to` = 'tuoikia@gmail.com'
WHERE `prfwj_contact_details`.`id` = 2;


UPDATE prfwj_contact_details
SET misc = REPLACE(misc, 'Sạp 38 Chợ đầu mối Bình Điền', 'B3-077 Chợ đầu mối Bình Điền')
WHERE `prfwj_contact_details`.`id` = 2;

UPDATE prfwj_contact_details
SET misc = REPLACE(misc, 'Số 1 Trương Đình Hội, Phường 1, Quận 8, TP. Hồ Chí Minh', 'B3-077 Chợ đầu mối Bình Điền')
WHERE `prfwj_contact_details`.`id` = 2;



UPDATE prfwj_contact_details
SET misc = REPLACE(misc, '0901.329.921', '0338117818')
WHERE `prfwj_contact_details`.`id` = 2;

UPDATE prfwj_contact_details
SET misc = REPLACE(misc, '0901329921', '0338117818')
WHERE `prfwj_contact_details`.`id` = 2;

UPDATE prfwj_content
SET  introtext = REPLACE(introtext, 'Sạp 38 Chợ đầu mối Bình Điền, Quận 7', 'B3-077 Chợ đầu mối Bình Điền');

UPDATE prfwj_content
SET  `fulltext` = REPLACE(`fulltext`, 'Sạp 38 Chợ đầu mối Bình Điền, Quận 7', 'B3-077 Chợ đầu mối Bình Điền');


UPDATE prfwj_content
SET  introtext = REPLACE(introtext, '0901.329.921', '0338117818');

UPDATE prfwj_content
SET  `fulltext` = REPLACE(`fulltext`, '0901.329.921', '0338117818');


UPDATE prfwj_eshop_products SET product_weight_id = 6, product_price=product_price/2  WHERE product_weight_id = 154;

UPDATE `prfwj_eshop_weightdetails` SET `weight_name` = '0.1Kg' WHERE `prfwj_eshop_weightdetails`.`id` = 157;

ALTER TABLE `prfwj_users` ADD `buy_all_time` TINYINT NOT NULL AFTER `type_user`;

UPDATE prfwj_eshop_products
SET product_sku = concat("",id);
