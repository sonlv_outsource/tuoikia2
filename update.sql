UPDATE prfwj_content
SET title = REPLACE(title, 'Minh Cầu Mart', 'Tươi Kìa');
UPDATE prfwj_content
SET alias = REPLACE(alias, 'minh-cau-mart', 'tuoi-kia');
UPDATE prfwj_content
SET  introtext = REPLACE(introtext, 'Minh Cầu Mart', 'Tươi Kìa');
UPDATE prfwj_content
SET fulltext = REPLACE(fulltext, 'minhcaumart.vn', 'tuoikia.com');

UPDATE prfwj_content
SET title = REPLACE(title, 'Minh Cau Mart', 'Tươi Kìa');

UPDATE prfwj_content
SET fulltext = REPLACE(fulltext, 'Minh Cau Mart', 'Tươi Kìa');
UPDATE prfwj_content
SET fulltext = REPLACE(fulltext, 'Minh Cầu Mart', 'Tươi Kìa');
UPDATE prfwj_content
SET introtext = REPLACE(introtext, 'Minh Cau Mart', 'Tươi Kìa');
UPDATE prfwj_content
SET fulltext = REPLACE(fulltext, 'Minh Cau', 'Tươi Kìa');


UPDATE prfwj_eshop_products
SET product_sku = concat("",id);

UPDATE prfwj_eshop_productdetails
SET product_desc = REPLACE(product_desc,'Đang cập nhật nè','Tươi Kìa chuyên phân phối bán sỉ - bán lẻ các loại rau củ quả');

UPDATE prfwj_eshop_productdetails
SET product_short_desc = REPLACE(product_short_desc,"Đang cập nhật nè","Tươi Kìa chuyên phân phối bán sỉ - bán lẻ các loại rau củ quả");
UPDATE prfwj_eshop_productdetails
SET product_short_desc = "Tươi Kìa chuyên phân phối bán sỉ - bán lẻ các loại rau củ quả";


UPDATE prfwj_eshop_productdetails
SET product_desc = REPLACE(product_desc,'tuoikia.com','carspahue.com');
UPDATE prfwj_eshop_productdetails
SET product_short_desc = REPLACE(product_short_desc,'tuoikia.com','carspahue.com');

UPDATE prfwj_eshop_productdetails
SET product_desc = REPLACE(product_desc,'Tươi Kìa','CarSpaHue');
UPDATE prfwj_eshop_productdetails
SET product_short_desc = REPLACE(product_short_desc,'Tươi Kìa','CarSpaHue');


UPDATE prfwj_content
SET title = REPLACE(title, 'tuoikia.com', 'carspahue.com');
UPDATE prfwj_content
SET title = REPLACE(title, 'Tươi Kìa', 'CarSpaHue');
UPDATE prfwj_content
SET alias = REPLACE(alias, 'Che3Mien', 'CarSpaHue');
UPDATE prfwj_content
SET  introtext = REPLACE(introtext, 'Tươi Kìa', 'CarSpaHue');
UPDATE prfwj_content
SET fulltext = REPLACE(fulltext, 'tuoikia.com', 'carspahue.com');
UPDATE prfwj_content
SET fulltext = REPLACE(fulltext, 'Atom Auto Services', 'CarSpaHue');

UPDATE prfwj_eshop_productdetails
SET product_short_desc = REPLACE(product_short_desc,"Atom Auto Services","CarSpaHue");
UPDATE prfwj_eshop_productdetails
SET product_desc = REPLACE(product_desc,"Atom Auto Services","CarSpaHue");

UPDATE prfwj_content
SET  introtext = REPLACE(introtext, 'Số 01, đường Minh Cầu, Phường Phan Đình Phùng, TP. Thái Nguyên, Thái Nguyên', '10/6/173 Bùi Thị Xuân, Huế, Thừa Thiên Huế');



UPDATE prfwj_content
SET  introtext = REPLACE(introtext, 'Sạp 38 Chợ đầu mối Bình Điền, Quận 7', 'B3-077 Chợ đầu mối Bình Điền');

UPDATE prfwj_content
SET  `fulltext` = REPLACE(`fulltext`, 'Sạp 38 Chợ đầu mối Bình Điền, Quận 7', 'B3-077 Chợ đầu mối Bình Điền');
