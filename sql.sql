/*
SQLyog Community v13.1.2 (64 bit)
MySQL - 5.5.27 : Database - bizappco_trasua
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

USE `bizappco_trasua`;

/*Table structure for table `prfwj_acymailing_action` */



/*Table structure for table `prfwj_eshop_home_group` */

DROP TABLE IF EXISTS `prfwj_eshop_home_group`;

CREATE TABLE `prfwj_eshop_home_group` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `prfwj_eshop_home_group` */

INSERT  INTO `prfwj_eshop_home_group`(`id`,`title`) VALUES
(1,'New arrival'),
(2,'Discount'),
(3,'Hot deal');

/*Table structure for table `prfwj_eshop_home_products` */

DROP TABLE IF EXISTS `prfwj_eshop_home_products`;

CREATE TABLE `prfwj_eshop_home_products` (
  `product_id` INT(11) NOT NULL,
  `group_id` INT(11) NOT NULL,
  `date_start` DATETIME DEFAULT NULL,
  `date_end` DATETIME DEFAULT NULL,
  PRIMARY KEY (`product_id`,`group_id`),
  KEY `group_id` (`group_id`),
  CONSTRAINT `prfwj_eshop_home_products_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `prfwj_eshop_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `prfwj_eshop_home_products_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `prfwj_eshop_home_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

/*Data for the table `prfwj_eshop_home_products` */

INSERT  INTO `prfwj_eshop_home_products`(`product_id`,`group_id`,`date_start`,`date_end`) VALUES
(48,1,'2019-06-23 11:08:51','2019-06-30 11:08:57'),
(48,2,'2019-06-23 11:09:11','2019-06-30 11:08:57'),
(49,1,'2019-06-23 11:09:11','2019-06-27 11:09:15'),
(49,2,'2019-06-23 11:09:11','2019-06-30 11:08:57'),
(50,1,'2019-06-23 11:09:11','2019-06-30 11:08:57'),
(50,3,'2019-06-23 11:09:11','2019-06-23 11:09:11');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

 CREATE TABLE `prfwj_eshop_stock` ( `id` INT(11) NOT NULL AUTO_INCREMENT, `name` VARCHAR(255), `address` VARCHAR(255), `country_id` INT(11), `zone_id` INT(11), PRIMARY KEY (`id`), FOREIGN KEY (`country_id`) REFERENCES `prfwj_eshop_countries`(`id`), FOREIGN KEY (`zone_id`) REFERENCES `prfwj_eshop_zones`(`id`) ) ENGINE=INNODB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
 CREATE TABLE `prfwj_eshop_stock_user` ( `stock_id` INT NOT NULL, `user_id` INT NOT NULL, PRIMARY KEY (`stock_id`, `user_id`), FOREIGN KEY (`stock_id`) REFERENCES `prfwj_eshop_stock`(`id`), FOREIGN KEY (`user_id`) REFERENCES `prfwj_users`(`id`) ) ENGINE=INNODB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

 CREATE TABLE `prfwj_eshop_stock_product` ( `stock_id` INT(11) NOT NULL, `product_id` INT(11) NOT NULL, `qty` INT(11), PRIMARY KEY (`stock_id`, `product_id`) ) ENGINE=INNODB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
ALTER TABLE `prfwj_eshop_stock_product` ADD FOREIGN KEY (`stock_id`) REFERENCES `prfwj_eshop_stock`(`id`), ADD FOREIGN KEY (`product_id`) REFERENCES `prfwj_eshop_products`(`id`);

ALTER TABLE `prfwj_eshop_orderproducts` ADD COLUMN `stock_id` INT(11) NULL AFTER `tax`, ADD COLUMN `status_id` INT(11) NULL AFTER `stock_id`, ADD COLUMN `modified_date` DATETIME NULL AFTER `status_id`, ADD COLUMN `modefied_by` INT NULL AFTER `modified_date`;

ALTER TABLE `prfwj_eshop_orderproducts` ADD COLUMN `note` TEXT NULL AFTER `modefied_by`;


INSERT  INTO `prfwj_eshop_stock`(`id`,`name`,`address`,`country_id`,`zone_id`) VALUES
(1,'Kho 1','abc',1,1),
(2,'Kho 2','abcd',1,10),
(3,'Kho 3','abcd e',1,4);

/*Data for the table `prfwj_eshop_stock_product` */

INSERT  INTO `prfwj_eshop_stock_product`(`stock_id`,`product_id`,`qty`) VALUES
(1,32,1000),
(1,33,1000),
(1,34,1000),
(2,32,500),
(2,33,500),
(2,34,500);

/*Data for the table `prfwj_eshop_stock_user` */

INSERT  INTO `prfwj_eshop_stock_user`(`stock_id`,`user_id`) VALUES
(1,759),
(1,2609);

ALTER TABLE `prfwj_eshop_orders` ADD `payment_status` TINYINT NOT NULL DEFAULT '0' AFTER `checked_out_time`;
ALTER TABLE `prfwj_users` ADD `address` VARCHAR(255) NOT NULL AFTER `email`, ADD `phone` VARCHAR(10) NOT NULL AFTER `address`;
ALTER TABLE `prfwj_users` ADD `is_stock_manager` TINYINT NOT NULL DEFAULT '0' AFTER `phone`, ADD `stock_id` INT NOT NULL AFTER `is_stock_manager`;

ALTER TABLE `prfwj_eshop_stock_user` ADD `is_stock_manager` TINYINT NOT NULL DEFAULT '0' AFTER `user_id`;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


ALTER TABLE `prfwj_eshop_home_group` ADD `ordering` INT(11) NOT NULL DEFAULT '0' AFTER `title`, ADD `published` INT(11) NOT NULL DEFAULT '1' AFTER `ordering`, ADD `group_type` VARCHAR(255) NOT NULL AFTER `published`;

UPDATE `prfwj_eshop_home_group` SET `group_type` = 'product_featured' WHERE `prfwj_eshop_home_group`.`id` = 1;
ALTER TABLE `prfwj_eshop_home_group` ADD `max_num` INT(11) NULL DEFAULT '10' AFTER `group_type`;
UPDATE `prfwj_eshop_home_group` SET `group_type` = 'discount' WHERE `prfwj_eshop_home_group`.`id` = 2;

DELETE FROM prfwj_eshop_home_group;

INSERT INTO `prfwj_eshop_home_group` (`id`, `title`, `ordering`, `published`, `group_type`, `max_num`) VALUES
(1, 'Sẩn phẩm bán chaỵ', 3, 1, 'product_featured', 10),
(2, 'Sản phẩm giảm giá', 2, 1, 'discount', 10),
(3, 'Deal hot', 4, 1, '', 10),
(4, 'Sản phẩm mới', 1, 1, '', 10);

ALTER TABLE `prfwj_store` ADD `email` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `adminstore`;

TRUNCATE prfwj_eshop_addresses;
TRUNCATE prfwj_eshop_stock;
TRUNCATE prfwj_eshop_stock_product;
TRUNCATE prfwj_eshop_stock_user;

ALTER TABLE `prfwj_eshop_orders` ADD `last_email` DATETIME  AFTER `payment_status`;


--------------------------------------------------
DROP TABLE IF EXISTS `prfwj_booking`;
CREATE TABLE `prfwj_booking` (
  `id` int(11) UNSIGNED NOT NULL,
  `ordering` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL,
  `status_id` int(11) NOT NULL,
  `checked_out` int(11) NOT NULL,
  `checked_out_time` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `phone` varchar(16) NOT NULL,
  `address` varchar(255) NOT NULL,
  `booking_date` datetime NOT NULL,
  `hours` varchar(255) NOT NULL,
  `note` text NOT NULL,
  `created_time` datetime DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `prfwj_eshop_products` CHANGE `product_weight` `product_weight` DECIMAL(15,2) NULL DEFAULT NULL;
ALTER TABLE `prfwj_eshop_productoptionvalues` CHANGE `price` `price` DECIMAL(15,0) NULL DEFAULT NULL;
ALTER TABLE `prfwj_eshop_productoptionvalues` CHANGE `weight` `weight` DECIMAL(15,2) NULL DEFAULT NULL;

ALTER TABLE `prfwj_eshop_orders` ADD `estimated_pick_time` DATETIME NOT NULL AFTER `last_email`, ADD `estimated_deliver_time` DATETIME NOT NULL AFTER `estimated_pick_time`, ADD `ref_fee` INT NOT NULL AFTER `estimated_deliver_time`;

alter table prfwj_eshop_orders add shipping_status int default 0 not null after shipping_eu_vat_number;



--- Update one page
INSERT INTO `prfwj_eshop_payments` ( `name`, `title`, `author`, `creation_date`, `copyright`, `license`, `author_email`, `author_url`, `version`, `description`, `params`, `ordering`, `published`) VALUES
('os_onepay', 'Thanh toán qua cổng onepage', 'Le Van Son', '0000-00-00 00:00:00', 'Copyright 2010-2019 BizappCo', 'http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU/GPL version 2', 'bizappco@gmail.com', 'appbanhang.com', '1.0', 'Onepay for EShop', '{\"gateway_domectic\":\"1\",\"gateway_international\":\"1\",\"token\":\"A3EFDFABA8653DF2342E8DAC29B51AF0\",\"enviroment\":\"dev\"}', 2, 1);


----Workflow status------------------------------------------------------

DROP TABLE IF EXISTS `prfwj_eshop_status_workflow`;

CREATE TABLE `prfwj_eshop_status_workflow` (
  `order_status` INT(11) NOT NULL,
  `next_status` INT(11) NOT NULL,
  PRIMARY KEY (`order_status`,`next_status`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4;

/*Data for the table `prfwj_eshop_status_workflow` */

INSERT  INTO `prfwj_eshop_status_workflow`(`order_status`,`next_status`) VALUES (8,1),(8,9),(8,10),(9,1),(9,4),(9,13),(10,1),(10,4),(10,9),(10,13),(13,4);







ALTER TABLE `prfwj_notification` ADD `show_app` INT NULL DEFAULT '0' AFTER `message`;
ALTER TABLE `prfwj_notification` ADD `page_app` VARCHAR(255) NOT NULL AFTER `show_app`, ADD `id_app` VARCHAR(255) NOT NULL AFTER `page_app`, ADD `para_1` VARCHAR(255) NOT NULL AFTER `id_app`, ADD `para_2` VARCHAR(255) NOT NULL AFTER `para_1`, ADD `para_3` VARCHAR(255) NOT NULL AFTER `para_2`;
ALTER TABLE `prfwj_notification` ADD `page_title_app` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `show_app`;

ALTER TABLE `prfwj_banners` ADD `page_title_app` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `version`;
ALTER TABLE `prfwj_banners` ADD `page_app` VARCHAR(255) NOT NULL AFTER `page_title_app`, ADD `id_app` INT NOT NULL AFTER `page_app`;


ALTER TABLE `prfwj_eshop_products` ADD `product_total_weight` FLOAT NOT NULL DEFAULT '0' AFTER `price_modified_date`;
ALTER TABLE `prfwj_eshop_products` ADD `product_price_discounted` FLOAT NOT NULL DEFAULT '0' AFTER `product_total_weight`;
ALTER TABLE `prfwj_eshop_products` ADD `product_discount` FLOAT NOT NULL DEFAULT '0' AFTER `product_price_discounted`;

ALTER TABLE `prfwj_eshop_products` ADD `product_discount_from_date` DATETIME NULL AFTER `product_discount`, ADD `product_discount_to_date` DATETIME NULL AFTER `product_discount_from_date`;

ALTER TABLE `prfwj_eshop_orders` ADD `delivery_hour` VARCHAR(255) NOT NULL AFTER `delivery_date`;


--- Add display type
ALTER TABLE `prfwj_eshop_home_group` ADD `list_type` VARCHAR(32) NOT NULL DEFAULT 'grid' AFTER `max_num`;
ALTER TABLE `prfwj_eshop_home_group` ADD `view_more` INT(1) NOT NULL DEFAULT '1' AFTER `list_type`;

UPDATE `prfwj_eshop_configs` SET `config_value` = 'TK' WHERE `prfwj_eshop_configs`.`config_key` = 'invoice_prefix';
