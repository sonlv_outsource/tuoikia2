<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 4/27/2019
 * Time: 9:51 AM
 */

use api\model\dao\ContentDao;
use api\model\dao\shop\ShopHomeProductDao;
use api\model\dao\shop\ShopCategoryDao;
use api\model\dao\shop\ShopWishlistDao;
use api\model\Sconfig;
use api\model\SUtil;
use Joomla\Registry\Registry;

defined('_JEXEC') or die('Restricted access');

class UsersApiResourceHome extends ApiResource
{
    static public function routes()
    {
        $routes[] = 'home/';

        return $routes;
    }

    public function delete()
    {
        $this->plugin->setResponse('in delete');
    }


    /**
     * @OA\Get(
     *     path="/api/users/home",
     *     tags={"User"},
     *     summary="Get home page",
     *     description="Get home page",
     *     operationId="get",
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Invalid request",
     *     )
     * )
     */
    public function get()
    {
        $config = new Sconfig();
        $result = array();
        $dao = new ContentDao();
        $params = array(
            'order' => 'id DESC',
            'limit' => 4
        );
        // $result['contents'] = $dao->getContent($params);
        /*$result['slide'] = array(
            array('thumb' => 'http://aloluatsu.bizappco.com/images/2017/1529916327_banner_1.jpg'),
            array('thumb' => 'http://aloluatsu.bizappco.com/images/2017/1529916327_banner_1.jpg'),
            array('thumb' => 'http://aloluatsu.bizappco.com/images/2017/1529916327_banner_1.jpg'),
            array('thumb' => 'http://aloluatsu.bizappco.com/images/2017/1529916327_banner_1.jpg')
        );*/
        $result['slide'] = $this->getBannerSlide();

        $result['categories'] = $this->getCategories();
        $userWishlist = $this->getWishlist();
        if ($userWishlist) {
            $wishList = array(
                'id' => -2,
                'title' => 'Sản phẩm yêu thích',
                'list_type' => $config->wishlist_type,
                'view_more' => 1,
                'products' => $userWishlist
            );
            if (count($userWishlist) > 10) {
                $result['modules'] = array($wishList);
            } else {
                $result['modules'] = array_values($this->getHomeProduct());
                array_unshift($result['modules'], $wishList);
            }
        } else {
            $result['modules'] = array_values($this->getHomeProduct());
        }

        // Sort cateogry
        foreach ($result['categories'] as $key => $firstLevel) {
            if ($firstLevel->child) {
                foreach ($firstLevel->child as $secondLevel) {
                    if (count($secondLevel->child)) {
                        $result['categories'][$key]->page = 'CategoriesPage';
                        break;
                    }
                }
            }
        }

        $this->plugin->setResponse($result);

    }

    public function getWishlist()
    {
        $userInfo = SUtil::getUserByToken();
        if ($userInfo) {
            $dao = new ShopWishlistDao();
            $params = array();
            $params['offset'] = 0;
            $params['limit'] = 20;
            $params['where'][] = 'w.customer_id = ' . (int)$userInfo['id'];

            $params['order'] = 'w.id DESC ';
            return $dao->getProducts($params);
        }
    }

    public function getBannerSlide()
    {
        require_once JPATH_SITE . '/modules/mod_bannerslider/helper.php';
        $db = JFactory::getDBO();
        $db->setQuery("SELECT * FROM `#__modules` WHERE module = 'mod_bannerslider' AND published = 1 ");
        $module = $db->loadObject();
        // Get module parameters
        $params = new Registry($module->params);
        $list = &modWalkswithmeBannerSlider::getList($params);

        $slides = array();
        if ($list) {
            $baseUrl = JUri::base();

            foreach ($list as $item) {
                $sql = 'SELECT id_app, page_app, page_title_app FROM #__banners  WHERE id = ' . (int)$item->id;
                $info = $db->setQuery($sql)->loadObject();
                $slides[] = array(
                    'thumb' => $baseUrl . $item->params->get('imageurl'),
                    'id' => $info->id_app,
                    'page' => $info->page_app,
                    'title' => $info->page_title_app,
                );
            }
        }

        return $slides;
    }

    private function getHomeProduct()
    {
        $params['join'][] =
            array(
                'type' => 'LEFT',
                'with_table' => '#__eshop_products AS p ON p.id = hp.product_id'
            );
        $params['join'][] =
            array(
                'type' => 'LEFT',
                'with_table' => '#__eshop_productdetails AS d ON p.id = d.product_id'
            );

        $params['join'][] =
            array(
                'type' => 'LEFT',
                'with_table' => '#__eshop_productcategories AS pc ON ( pc.product_id = p.id)'
            );
        $params['where'][] = 'd.language = \'vi-VN\'';
        $dao = new ShopHomeProductDao();
        return $dao->getProducts($params);

    }

    private function getCategories()
    {
        $dao = new ShopCategoryDao();
        $params = array();
        $params['offset'] = 0;
        $params['limit'] = 200;

        $params['where'][] = 'd.language = \'vi-VN\'';
        $params['order'] = 'c.level ASC, c.ordering ASC';

        return $dao->getCategories($params);

    }


    private function getHomeBannerIcon()
    {
        return array(
            array(
                'id' => 28,
                'key' => 'ContentPage',
                'title' => 'Homestay',
                'text' => 'Homestay - dịch vụ nghỉ dưỡng',
                'image' => 'http://carspahue.com/images/banners/mobile_1.jpg'
            ),
            array(
                'id' => 19,
                'title' => 'Dịch vu chăm sóc - cứu hộ ô tô',
                'key' => 'ContentCategoriesPage',
                'text' => 'Dịch vu chăm sóc - cứu hộ ô tô',
                'image' => 'http://carspahue.com/images/banners/mobile_2.jpg'
            ),
            array(
                'key' => 'CategoriesPage',
                'text' => 'Sản phẩm - Phụ tùng ô tô',
                'image' => 'http://carspahue.com/images/banners/mobile_3.jpg'
            ),
        );
    }


}
