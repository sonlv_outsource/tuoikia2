<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 4/27/2019
 * Time: 9:51 AM
 */

use api\model\services\Onesignal;
use api\model\SUtil;
use Joomla\CMS\Date\Date;

defined('_JEXEC') or die('Restricted access');
require_once(JPATH_SITE . '/components/com_eshop/helpers/helper.php');
require_once(JPATH_SITE . '/administrator/components/com_eshop/libraries/rad/form/form.php');
require_once(JPATH_SITE . '/administrator/components/com_eshop/libraries/rad/validator/validator.php');
require_once(JPATH_SITE . '/components/com_eshop/helpers/currency.php');
require_once(JPATH_SITE . '/components/com_eshop/helpers/tax.php');
require_once(JPATH_SITE . '/components/com_eshop/helpers/html.php');

class UsersApiResourceTest extends ApiResource
{
    /**
     * @OA\Post(
     *     path="/api/users/changepassword",
     *     tags={"User"},
     *     summary="Change password user",
     *     description="Change password user",
     *     operationId="post",
     *     security = { { "bearerAuth": {} } },
     *     @OA\RequestBody(
     *         required=true,
     *         description="Change password",
     *         @OA\JsonContent(ref="#/components/schemas/ChangePasswordForm"),
     *         @OA\MediaType(
     *            mediaType="multipart/form-data",
     *            @OA\Schema(ref="#/components/schemas/ChangePasswordForm"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful login",
     *         @OA\Schema(ref="#/components/schemas/ErrorModel"),
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Invalid request",
     *     )
     * )
     */
    public function get()
    {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);


        $timezone = JFactory::getUser()->getTimezone();
        $date = JFactory::getDate();



        echo date("Y/m/d H:i:s");
        echo gmdate("Y/m/d H:i:s");




        $given = new DateTime('2020-04-06 00:00:00', $timezone);
        $given->setTimezone(new DateTimeZone("UTC"));
       echo  $given->format("Y-m-d H:i:s");

        echo '<br />';

        print_r($date->format('d/m/Y H:i'));
        echo '<br />';
        print_r($date->format('d/m/Y H:i', $timezone));


        $date->setTimezone($timezone);
        echo $date->format('d/m/Y H:i:s');
        //$sender = new Onesignal();
        //$sender->sendMessage();
        //EshopHelper::makeOrderXml(187);
        //Update address long lat

        /*$db = JFactory::getDbo();
        $sql = 'SELECT * FROM #__eshop_addresses WHERE (lng is null or lat is null) AND country_id is not null ';
        $address = $db->setQuery($sql)->loadObjectList();
        if($address){
            foreach ($address as $add){
                $fullAddress = SUtil::getFullAddress($add->id);
                $longlatInfo = SUtil::getLonLatFromAddress($fullAddress);
                $add->lat = $longlatInfo['lat'];
                $add->lng = $longlatInfo['lng'];
                $result = $db->updateObject('#__eshop_addresses', $add, 'id');
            }
        }*/

        $db = JFactory::getDbo();
        $id = 129;
        $sql = 'SELECT * FROM #__eshop_orders WHERE id = '.$id;
        $row = $db->setQuery($sql)->loadObject();
        //print_r($row);die;


        //EshopHelper::completeOrder($row);
        //Send confirmation email here
        if (EshopHelper::getConfigValue('order_alert_mail'))
        {
            EshopHelper::testMailContent($row);
        }
        die;

    }

    

}
