<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 4/27/2019
 * Time: 9:51 AM
 */

use api\model\dao\UserDao;
use api\model\dao\shop\ShopWishlistDao;
use api\model\dao\shop\ShopCategoryDao;
use api\model\form\ChangePasswordForm;
use api\model\Sconfig;

defined('_JEXEC') or die('Restricted access');
jimport('joomla.user.user');

class UsersApiResourceAddwishlist extends ApiResource
{
    static public function routes()
    {
        $routes[] = 'addwishlist/';

        return $routes;
    }

    /**
     * @OA\Post(
     *     path="/api/users/shopproducts",
     *     tags={"User"},
     *     summary="Change password user",
     *     description="Change password user",
     *     operationId="post",
     *     @OA\RequestBody(
     *         required=true,
     *         description="Change password",
     *         @OA\JsonContent(ref="#/components/schemas/ProductForm"),
     *         @OA\MediaType(
     *            mediaType="multipart/form-data",
     *            @OA\Schema(ref="#/components/schemas/ProductForm"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful login",
     *         @OA\Schema(ref="#/components/schemas/ErrorModel"),
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Invalid request",
     *     )
     * )
     */


    public function post()
    {
        $data = $this->getRequestData();
        $user = JFactory::getUser();
        if ($user->id && $data['id']) {
            $db = JFactory::getDbo();
            $sql = 'SELECT id FROM #__eshop_wishlists WHERE 	customer_id = ' . (int)$user->id . ' AND product_id = ' . (int)$data['id'];
            $id = $db->setQuery($sql)->loadResult();
            if (!$id) {
                $obj = new stdClass();
                $obj->customer_id = $user->id;
                $obj->product_id = $data['id'];
                $db->insertObject('#__eshop_wishlists', $obj, 'id');

            }
            $this->plugin->setResponse('Thêm sản phẩm vào danh sách yêu thích thành công.');
            return true;
        }
        ApiError::raiseError('101', 'Vui lòng đăng nhập.');
        return false;
    }
}
