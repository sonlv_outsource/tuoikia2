<?php
/**
 * Created by PhpStorm.
 * User: lvson
 * Date: 4/26/2019
 * Time: 10:50 AM
 */

namespace api\model\biz\shop;

use api\model\AbtractBiz;
use api\model\SUtil;

/**
 * @OA\Schema(required={"id", "title"}, @OA\Xml(name="ShopOrderBiz"))
 */
class ShopOrderBiz extends AbtractBiz
{
    /**
     * @OA\Property(format="id")
     * @var int
     */
    public $id;
    /**
     * @OA\Property(format="order_number")
     * @var string
     */
    public $order_number;
    /**
     * @OA\Property(format="created_date")
     * @var string
     */
    public $created_date;
    /**
     * @OA\Property(format="total")
     * @var string
     */
    public $total;
    /**
     * @OA\Property(format="orderstatus_name")
     * @var string
     */
    public $orderstatus_name;
    /**
     * @OA\Property(format="pay_note")
     * @var string
     */
    public $pay_note;

    public $payment_status;

    public $canCancel = false;
    public $delivery_hour = '';

    public function setAttributes($data)
    {
        $data['canCancel'] = SUtil::canCancelOrder($data['order_status_id'], $data['payment_status']);
        $data['created_date'] = date('d/m/Y H:i', strtotime($data['created_date'] . ' +7 hours'));
        $deli = date('d/m/Y', strtotime($data['delivery_date'] . ' +7 hours'));
        if ($data['delivery_hour'] != '') {
            $data['delivery_hour'] = $deli . ', ' . $data['delivery_hour'];
        } else {
            $data['delivery_hour'] = $deli;
        }


        if ($data['payment_status'] == 1) {
            $data['payment_status'] = 'Đã thanh toán';
        } else {
            switch ($data['payment_method']) {
                case 'os_onepay':
                    $data['payment_status'] = 'Chưa thanh toán ' . $this->getPaymentError($data['payment_code']);
                    break;
                default:
                    $data['payment_status'] = 'Chưa thanh toán';
            }
        }


        parent::setAttributes($data);
    }

    function getPaymentError($responseCode)
    {

        switch ($responseCode) {
            case "0" :
                $result = "Giao dịch thành công";
                break;
            case "1" :
                $result = "Ngân hàng từ chối giao dịch";
                break;
            case "3" :
                $result = "Mã đơn vị không tồn tại";
                break;
            case "4" :
                $result = "Không đúng access code";
                break;
            case "5" :
                $result = "Số tiền không hợp lệ";
                break;
            case "6" :
                $result = "Mã tiền tệ không tồn tại";
                break;
            case "7" :
                $result = "Lỗi không xác định";
                break;
            case "8" :
                $result = "Số thẻ không đúng";
                break;
            case "9" :
                $result = "Tên chủ thẻ không đúng";
                break;
            case "10" :
                $result = "Thẻ hết hạn/Thẻ bị khóa";
                break;
            case "11" :
                $result = "Thẻ chưa đăng ký sử dụng dịch vụ";
                break;
            case "12" :
                $result = "Ngày phát hành/Hết hạn không đúng";
                break;
            case "13" :
                $result = "Vượt quá hạn mức thanh toán";
                break;
            case "21" :
                $result = "Số tiền không đủ để thanh toán";
                break;
            case "99" :
                $result = "Người sủ dụng hủy giao dịch";
                break;
            default :
                $result = "Giao dịch thất bại";
        }
        return ' - (' . $result . ')';
    }
}
