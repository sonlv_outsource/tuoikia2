<?php
/**
 * Created by PhpStorm.
 * User: lvson
 * Date: 4/26/2019
 * Time: 10:53 AM
 */

namespace api\model\dao\shop;

use api\model\AbtractDao;
use api\model\biz\shop\ShopPaymentBiz;

class ShopPaymentDao extends AbtractDao
{
    public $select = array(
        'id',
        'name',
        'title',
        'params'
    );

    public function getTable()
    {
        return '#__eshop_payments';
    }

    public function getPayments($params = array())
    {
        $paramsDefault = array(
            'select' => $this->select,
            'where' => array(
                'published = 1'
            ),
            'order' => 'ordering ASC'
        );
        if (isset($params['where']) && $params['where']) {
            foreach ($params['where'] as $item) {
                $paramsDefault['where'][] = $item;
            }
        }
        if ($params) {
            foreach ($params as $k => $item) {
                if ($k === 'where') {
                    continue;
                }
                $paramsDefault[$k] = $item;
            }
        }
        $result = $this->getList($paramsDefault);
        $list = array();
        if ($result) {
            foreach ($result as $item) {
                if ($item['name'] == 'os_onepay') {
                    $params = json_decode($item['params'], true);
                    if ($params['gateway_domestic'] == 1) {
                        if (
                            (!empty($params['domestic_merchant_id'])
                                && !empty($params['domestic_access_code'])
                                && !empty($params['domestic_token']))
                            || $params['enviroment'] == 'dev'
                        ) {
                            $biz = new ShopPaymentBiz();
                            $biz->setAttributes(array(
                                'id' => $item['id'] . '_domestic',
                                'name' => 'os_onepay',
                                'title' => $params['domestic_name'],
                            ));
                            $list[] = $biz;
                        }

                    }
                    if ($params['gateway_international'] == 1) {
                        if (

                            (!empty($params['international_merchant_id'])
                                && !empty($params['international_access_code'])
                                && !empty($params['international_token']))
                            || $params['enviroment'] == 'dev'
                        ) {
                            $biz = new ShopPaymentBiz();
                            $biz->setAttributes(array(
                                'id' => $item['id'] . '_international',
                                'name' => 'os_onepay',
                                'title' => $params['international_name'],
                            ));
                            $list[] = $biz;
                        }
                    }
                } else {
                    $biz = new ShopPaymentBiz();
                    $biz->setAttributes($item);
                    $list[] = $biz;
                }

            }
        }
        return $list;
    }

    public function getPaymentInfo($params)
    {
        $paramsDefault = array(
            'select' => $this->select,
            'where' => array(),
            'order' => 'ordering ASC'
        );
        if (isset($params['where']) && $params['where']) {
            foreach ($params['where'] as $item) {
                $paramsDefault['where'][] = $item;
            }
        }
        if ($params) {
            foreach ($params as $k => $item) {
                if ($k === 'where') {
                    continue;
                }
                $paramsDefault[$k] = $item;
            }
        }
        return $this->get($paramsDefault);

    }


}
