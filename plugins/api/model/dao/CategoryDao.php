<?php
/**
 * Created by PhpStorm.
 * User: lvson
 * Date: 4/26/2019
 * Time: 10:53 AM
 */

namespace api\model\dao\shop;

use api\model\AbtractDao;
use api\model\biz\CategoryBiz;
use api\model\biz\ProjectBiz;

class CategoryDao extends AbtractDao
{
    public $select = array(
        'id',
        'title',
        'short_description',
        'description',
        'file_1',
        'file_2',
        'file_3',
        'file_4',
        'file_5'
    );

    public function getTable()
    {
        return '#__categories';
    }

    public function getPrice($params = array())
    {
        $paramsDefault = array(
            'select' => array('note'),
            'where' => array(
                'id = '.(int)$params['cat_id']
            )
        );
        $result = $this->get($paramsDefault);
        return $result['note'];
    }




}
