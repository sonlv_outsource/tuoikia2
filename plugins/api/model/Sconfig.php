<?php
/**
 * @package     api\model
 * @subpackage
 *
 * @copyright   A copyright
 * @license     A "Slug" license name e.g. GPL2
 */

namespace api\model;


class Sconfig
{
    public $hotline = '0338117818';
    public $siteName = 'Tươi Kìa!';
    public $deeplink = 'tuoikia';
    public $onesignalAppKey = 'dff7e1db-f5da-45f7-998b-3bab520bb7ad';
    public $onesignalRestKey = 'ZDg5N2U1YzMtOThjOS00ODJhLThmZGYtNzIxMWZiOTExZDgw';
    public $address = 'B3-077 Chợ đầu mối Bình Điền';
    public $service = array(
        array('text' => 'Được kiểm tra hàng trước khi thanh toán', 'icon' => 'ios-checkmark'),
        array('text' => 'Được tích điểm giảm giá', 'icon' => 'ios-checkmark'),
        array('text' => 'Giao hàng nhanh chóng', 'icon' => 'ios-checkmark')
    );
    public $wishlist_type = 'list';
    public $orderBeginStatus = 8;
    public $orderCancleStatus = 1;
    public $canCancelStatus = array(
        '8'
    );

    public $shippingLimitArea = array(
        -1
    );
    public $shippingLimitCategory = array();
    // public $shippingLimitCategory = array(
    //     3,
    //     213,
    //     214,
    //     29,
    //     215,
    //     216,
    //     217,
    //     30,
    //     161,
    //     159,
    //     278,
    //     279,
    //     280,
    //     281,
    //     284,
    //     285,
    //     286,
    //     287,
    //     288,
    //     289,
    //     360,
    //     294,
    //     295,
    //     297,
    //     298,
    //     299,
    //     301,
    //     303
    // );

    public $shippingLimitMessage = 'Sản phẩm dành riêng cho khu vực Quận 11';
    public $minCartAmount = 1;
    public $google_api_key = 'AIzaSyDh1rJHvJAmUFKjvCXpzefDp15lfdpT8So';
    public $shop_longlat = array(
        'lat' => 21.5930964802915,
        'lng' => 105.8363773802915
    );

    public $shipping_fee = 'Miễn phí vận chuyển đơn hàng từ 200.000đ. <br>Đơn hàng dưới 200.000 đ phí vận chuyển là 10.000 đ.';

    public $order_limit = true;
    public $order_begin = '06:00';
    public $order_end = '24:00';
    public $show_wishlist = true;
    public $weightList = array( 6, 153);
    public $gram = 153;

}
