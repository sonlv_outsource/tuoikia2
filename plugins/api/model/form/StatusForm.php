<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 4/27/2019
 * Time: 8:01 AM
 */

namespace api\model\form;

use api\model\AbtractForm;

/**
 * @OA\Schema(required={"customer_id, status_id"}, @OA\Xml(name="StatusForm"))
 */
class StatusForm extends AbtractForm
{
    /**
     * @OA\Property(example="id")
     * @var int
     */
    public $id;

    /**
     * @OA\Property(example="status_id")
     * @var string
     */
    public $status_id;

    /**
     * @OA\Property(example="total_revenue")
     * @var int
     */
    public $total_revenue;



    public function rule()
    {
        $rule = array(
            'required' => array(
                'id',
                'status_id'
            )
        );
        return $rule;
    }

}