<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * User view class.
 *
 * @since  1.5
 */
class UsersViewUser extends JViewLegacy
{
	protected $form;

	protected $item;

	protected $grouplist;

	protected $groups;

	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 *
	 * @since   1.5
	 */
	public function display($tpl = null)
	{
		$this->form      = $this->get('Form');
		$this->item      = $this->get('Item');
		$this->state     = $this->get('State');
		$this->tfaform   = $this->get('Twofactorform');
		$this->otpConfig = $this->get('otpConfig');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors), 500);
		}

		// Prevent user from modifying own group(s)
		$user = JFactory::getUser();

		if ((int) $user->id != (int) $this->item->id || $user->authorise('core.admin'))
		{
			$this->grouplist = $this->get('Groups');
			$this->groups    = $this->get('AssignedGroups');
		}

		$this->form->setValue('password', null);
		$this->form->setValue('password2', null);

		parent::display($tpl);
		$this->addToolbar();
	}

	public function getListStock()
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('s.*');
		$query->from('`#__store` AS s');
		$query->where($db->quoteName('state') . ' = 1');
		$query->order($db->quoteName('s.title') . ' ASC');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result;
	}

	public function getStockName($stock_id)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('s.*');
		$query->from('`#__store` AS s');
		$query->where('s.id = '.$stock_id);
		$db->setQuery($query);
		$result = $db->loadObject();
		return $result;
	}

	public function getProvinceName($pro_id)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('ec.*');
		$query->from('`#__eshop_countries` AS ec');
		$query->where('ec.id = '.$pro_id);
		$db->setQuery($query);
		$result = $db->loadObject();
		return $result->country_name;
	}

	/**
	 *
	 * Function to get addresses object list for user
	 * @return array object list
	 */
	public function getAddresses($userid)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('a.*, b.country_name, c.zone_name')
			->from('#__eshop_addresses AS a')
			->leftJoin('#__eshop_countries AS b ON (a.country_id = b.id)')
			->leftJoin('#__eshop_zones AS c ON (a.zone_id = c.id)')
			->where('a.customer_id = ' . (int)$userid);
		$db->setQuery($query);

		return $db->loadObjectList();
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since   1.6
	 */
	protected function addToolbar()
	{
		JFactory::getApplication()->input->set('hidemainmenu', true);

		$user      = JFactory::getUser();
		$canDo     = JHelperContent::getActions('com_users');
		$isNew     = ($this->item->id == 0);
		$isProfile = $this->item->id == $user->id;

		JToolbarHelper::title(
			JText::_(
				$isNew ? 'COM_USERS_VIEW_NEW_USER_TITLE' : ($isProfile ? 'COM_USERS_VIEW_EDIT_PROFILE_TITLE' : 'COM_USERS_VIEW_EDIT_USER_TITLE')
			),
			'user ' . ($isNew ? 'user-add' : ($isProfile ? 'user-profile' : 'user-edit'))
		);

		if ($canDo->get('core.edit') || $canDo->get('core.create'))
		{
			JToolbarHelper::apply('user.apply');
			JToolbarHelper::save('user.save');
		}

		if ($canDo->get('core.create') && $canDo->get('core.manage'))
		{
			JToolbarHelper::save2new('user.save2new');
		}

		if (empty($this->item->id))
		{
			JToolbarHelper::cancel('user.cancel');
		}
		else
		{
			JToolbarHelper::cancel('user.cancel', 'JTOOLBAR_CLOSE');
		}

		JToolbarHelper::divider();
		JToolbarHelper::help('JHELP_USERS_USER_MANAGER_EDIT');
	}
}
