<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the component HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('behavior.formvalidator');
JHtml::_('formbehavior.chosen', 'select');


$user   = JFactory::getUser($this->item->id);
$groups = $user->get('groups');
$is_staff_group = 0;
foreach ($groups as $group)
{
    //echo '<p>Group = ' . $group . '</p>';
		if($group == 11){
				$is_staff_group = 1;
		}
}

JFactory::getDocument()->addScriptDeclaration("
	Joomla.submitbutton = function(task)
	{
		if (task == 'user.cancel' || document.formvalidator.isValid(document.getElementById('user-form')))
		{
			Joomla.submitform(task, document.getElementById('user-form'));
		}
	};

	Joomla.twoFactorMethodChange = function(e)
	{
		var selectedPane = 'com_users_twofactor_' + jQuery('#jform_twofactor_method').val();

		jQuery.each(jQuery('#com_users_twofactor_forms_container>div'), function(i, el) {
			if (el.id != selectedPane)
			{
				jQuery('#' + el.id).hide(0);
			}
			else
			{
				jQuery('#' + el.id).show(0);
			}
		});
	};
");

// Get the form fieldsets.
$fieldsets = $this->form->getFieldsets();
?>

<form action="<?php echo JRoute::_('index.php?option=com_users&layout=edit&id=' . (int) $this->item->id); ?>" method="post" name="adminForm" id="user-form" class="form-validate form-horizontal" enctype="multipart/form-data">

	<?php echo JLayoutHelper::render('joomla.edit.item_title', $this); ?>

	<fieldset>
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'details')); ?>

			<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'details', JText::_('COM_USERS_USER_ACCOUNT_DETAILS')); ?>
				<div class="user-left">
				<?php foreach ($this->form->getFieldset('user_details') as $field) : ?>

					<div class="control-group group-<?php echo $field->fieldname; ?> <?php if($is_staff_group == 0 && ($field->fieldname == 'stock_id' || $field->fieldname == 'is_stock_manager')){ ?>is_staff<?php } ?>">
						<div class="control-label">
							<?php echo $field->label; ?>
						</div>
						<div class="controls">
							<?php if ($field->fieldname == 'password') : ?>
								<?php // Disables autocomplete ?> <input type="password" style="display:none">
							<?php endif; ?>
							<?php echo $field->input; ?>
							<?php if ($field->fieldname == 'stock_id') : ?><button id="choose-stock" type="button" class="btn btn-warning" onclick="chooseStock()">Chọn Kho</button>
								<!-- &nbsp;&nbsp;<span id="showalert">Click chọn nhóm "Nhân Viên Kho" để có thể chọn Kho</span> -->
								<?php if($_GET['id'] == 0 ){ ?>
									<br><br><span id="namestock"></span>
							<?php } ?>
								<?php if($this->item->stock_id > 0){ ?> <br><br><?php  $stock = $this->getStockName($this->item->stock_id); echo $stock->title;  ?> - <?php echo $this->getProvinceName($stock->province); ?> <?php } ?>

							<?php endif; ?>

						</div>
					</div>
				<?php endforeach; ?>
				</div>
				<div class="user-right">
					<h3>Danh sách địa chỉ</h3>
					<?php
					$listAddress =   $this->getAddresses($this->item->id);
					// echo "<pre>";
					// print_r($listAddress);
					// echo "</pre>";
					if(count($listAddress) > 0){
						foreach($listAddress as $index=>$address){
							if($address->address_1 != '' && $address->country_name != '' && $address->zone_name != ''){
								echo '<div class="address-item"><b>'.$index.'.</b>';
								echo '<div class="address-name">'.$address->firstname.' - '.$address->telephone.'</div>';
								echo '<div class="address-detail">'.$address->address_1.' , '.$address->zone_name.' , '.$address->country_name.'</div>';
								echo '</div>';
							}
						}
					}

					?>
				</div>
			<?php echo JHtml::_('bootstrap.endTab'); ?>

			<?php if ($this->grouplist) : ?>
				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'groups', JText::_('COM_USERS_ASSIGNED_GROUPS')); ?>
					<?php echo $this->loadTemplate('groups'); ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>
			<?php endif; ?>

			<?php
			$this->ignore_fieldsets = array('user_details');
			echo JLayoutHelper::render('joomla.edit.params', $this);
			?>

		<?php if (!empty($this->tfaform) && $this->item->id) : ?>
		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'twofactorauth', JText::_('COM_USERS_USER_TWO_FACTOR_AUTH')); ?>
		<div class="control-group">
			<div class="control-label">
				<label id="jform_twofactor_method-lbl" for="jform_twofactor_method" class="hasTooltip"
						title="<?php echo '<strong>' . JText::_('COM_USERS_USER_FIELD_TWOFACTOR_LABEL') . '</strong><br />' . JText::_('COM_USERS_USER_FIELD_TWOFACTOR_DESC'); ?>">
					<?php echo JText::_('COM_USERS_USER_FIELD_TWOFACTOR_LABEL'); ?>
				</label>
			</div>
			<div class="controls">
				<?php echo JHtml::_('select.genericlist', Usershelper::getTwoFactorMethods(), 'jform[twofactor][method]', array('onchange' => 'Joomla.twoFactorMethodChange()'), 'value', 'text', $this->otpConfig->method, 'jform_twofactor_method', false); ?>
			</div>
		</div>
		<div id="com_users_twofactor_forms_container">
			<?php foreach ($this->tfaform as $form) : ?>
			<?php $style = $form['method'] == $this->otpConfig->method ? 'display: block' : 'display: none'; ?>
			<div id="com_users_twofactor_<?php echo $form['method'] ?>" style="<?php echo $style; ?>">
				<?php echo $form['form'] ?>
			</div>
			<?php endforeach; ?>
		</div>

		<fieldset>
			<legend>
				<?php echo JText::_('COM_USERS_USER_OTEPS'); ?>
			</legend>
			<div class="alert alert-info">
				<?php echo JText::_('COM_USERS_USER_OTEPS_DESC'); ?>
			</div>
			<?php if (empty($this->otpConfig->otep)) : ?>
			<div class="alert alert-warning">
				<?php echo JText::_('COM_USERS_USER_OTEPS_WAIT_DESC'); ?>
			</div>
			<?php else : ?>
			<?php foreach ($this->otpConfig->otep as $otep) : ?>
			<span class="span3">
				<?php echo substr($otep, 0, 4); ?>-<?php echo substr($otep, 4, 4); ?>-<?php echo substr($otep, 8, 4); ?>-<?php echo substr($otep, 12, 4); ?>
			</span>
			<?php endforeach; ?>
			<div class="clearfix"></div>
			<?php endif; ?>
		</fieldset>

		<?php echo JHtml::_('bootstrap.endTab'); ?>
		<?php endif; ?>

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>
	</fieldset>

	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_('form.token'); ?>
</form>



<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="myModalLabel">GÁN KHO</h3>
            </div>
            <div class="modal-body">
                <div class="form-control-lg">
                    <div class="form-group row-fluid">
                        <label for="shipper" class="form-control-label span2">&nbsp;&nbsp;<strong>Chọn Kho:</strong></label>
		                <?php
		                $list = $this->getListStock();
		                ?>
                       <div class="span10">
                           <select name="shipper" id="shipper">
                               <option value="0">Vui lòng chọn</option>
		                       <?php foreach ($list as $item){?>
                                   <option value="<?php echo $item->id;?>"><?php echo $item->title;?> - <?php echo $this->getProvinceName($item->province); ?></option>
		                       <?php }?>
                           </select>
                           <div class="has-error error-shipper" style="color:red;"></div>
                       </div>
											 <input type="hidden" id="idrequest" name="idrequest" value="<?php echo $this->item->id; ?>">
											 <input type="hidden" id="is_stock_manager" name="is_stock_manager" value="<?php echo $this->item->is_stock_manager; ?>">

                    </div>

                </div>
                <div class="has-error error-msg"></div>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="saveChange()" id="saveChange" class="btn btn-success">Lưu lại</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>
<script>
js = jQuery.noConflict();

// js(document).ready(function () {
// 	<?php if($_GET['id'] > 0 ){ ?>
// 	if (js('#1group_11').is(":checked"))
// 	{
// 		js('#choose-stock').attr("disabled",false);
// 		js('#jform_is_stock_manager0').attr('disabled',false);
// 		js('#jform_is_stock_manager1').attr('disabled',false);
//
// 		js('#showalert').css("display","none");
//
// 	}else{
// 		//js('#jform_is_stock_manager input').attr('disabled','disabled');
// 		//js("#jform_is_stock_manager0").attr('disabled',true);
// 		js('#choose-stock').attr("disabled",true);
// 		js('#jform_is_stock_manager0').attr('disabled',true);
// 		js('#jform_is_stock_manager1').attr('disabled',true);
// 		js('#showalert').css("display","block");
//
// 	}
// 	<?php }else{ ?>
// 		js('#1group_2').prop('checked', false);
// 		js('#1group_11').prop('checked', true);
// 		//js('#jform_stock_id').val(100);
// 	<?php } ?>
//
// 	js('input[name="jform[groups][]"]').click(function(){
// 		js('#1group_2').prop('checked', false);
// 		js('#1group_11').prop('checked', false);
// 		js('#1group_10').prop('checked', false);
// 		js('#1group_'+js(this).val()).prop('checked', true);
// 		if(js(this).val() == 11 && js(this).is(":checked")){
// 				js('#choose-stock').attr("disabled",false);
// 				js('#showalert').css("display","block");
// 				js('#jform_is_stock_manager0').attr('disabled',false);
// 				js('#jform_is_stock_manager1').attr('disabled',false);
// 				js('#showalert').css("display","none");
// 		}else{
// 				js('#choose-stock').attr("disabled",true);
// 				js('#showalert').css("display","none");
// 				js('#jform_is_stock_manager0').attr('disabled',true);
// 				js('#jform_is_stock_manager1').attr('disabled',true);
// 				js('#showalert').css("display","block");
// 		}
// 	});
// });


var myModal = js('#myModal');
function chooseStock(){
	myModal.modal('show');
}

function saveChange () {

		var valid = true;
		var idShipper = jQuery('#shipper').val();
		var idRequest = jQuery('#idrequest').val();
		var is_stock_manager = jQuery('#is_stock_manager').val();
		if (idShipper == '0'){
				js('.error-shipper').html('Bạn vui lòng chọn Kho!');
				valid = false;
		}else {
				js('.error-shipper').html('');
				valid = true;
		}
		<?php if($_GET['id'] == 0 ){ ?>
			jQuery('#jform_stock_id').val(jQuery('#shipper').val());
			jQuery('#namestock').html(jQuery( "#shipper option:selected" ).text());
			myModal.modal('hide');
		<?php } ?>

		<?php if($_GET['id'] > 0 ){ ?>
		if (valid == true){
			js('.error-shipper').html('');
			js.ajax({
					url: "<?php echo JURI::root(); ?>administrator/index.php?option=com_users&task=user.assignStock&tmpl=component",
					method: 'POST',
					data:{idShipper: idShipper, idRequest: idRequest,is_stock_manager:is_stock_manager} ,
					success: function (result) {
							if(result == '1') {
									alert('Gán Kho thành công!');

									window.setTimeout('location.reload()', 300);

							}else{
									js('.error-msg').html('Có lỗi xảy ra, vui lòng thử lại!');
							}
					}
			});
		}
		<?php } ?>

	}
</script>

<style>
#myModal{width:50%;}
#myModal .chzn-container{width:80%!important;}
.user-left{
	float:left;
	width: 48%
}
.user-right{
	float:left;
	width: 48%
}
.address-item{
	padding-bottom: 15px;
}













/* label.checkbox[for="1group_1"]
{
  display:none!important;
}
label.checkbox[for="1group_3"]
{
  display:none!important;
}
label.checkbox[for="1group_4"]
{
  display:none!important;
}
label.checkbox[for="1group_9"]
{
  display:none!important;
}
label.checkbox[for="1group_6"]
{
  display:none!important;
}
label.checkbox[for="1group_5"]
{
  display:none!important;
}
label.checkbox[for="1group_7"]
{
  display:none!important;
}
#groups.tab-pane .control-group{margin-bottom: 0px!important;} */
</style>
