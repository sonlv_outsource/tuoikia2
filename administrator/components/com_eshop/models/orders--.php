<?php
/**
 * @version		3.1.0
 * @package		Joomla
 * @subpackage	EShop
 * @author  	Giang Dinh Truong
 * @copyright	Copyright (C) 2012 Ossolution Team
 * @license		GNU/GPL, see LICENSE.php
 */
// no direct access
defined('_JEXEC') or die();

/**
 * Eshop Component Model
 *
 * @package		Joomla
 * @subpackage	EShop
 * @since 1.5
 */
class EShopModelOrders extends EShopModelList
{
	function __construct($config)
	{
		$config['search_fields'] = array('order_number', 'firstname', 'lastname', 'email', 'payment_firstname', 'payment_lastname', 'shipping_firstname', 'shipping_lastname');
		$config['state_vars'] = array( 
			'filter_order_Dir' => array('DESC', 'cmd', 1));
		parent::__construct($config);
	}
	
	/**
	 * Basic build Query function.
	 * The child class must override it if it is necessary
	 *
	 * @return string
	 */
	public function _buildQuery()
	{
		$db = $this->getDbo();
		$state = $this->getState();
		$query = $db->getQuery(true);
        $date = JFactory::getDate()->toSql();
		$query->select('a.*, IF(a.payment_status = 0 AND datediff( \'' . $date . '\', a.created_date) > '.ESHOP_PAYMENT_OVER.', 1, 0) as pay_note, IF(a.payment_status = 0  AND ( a.last_email is NULL OR datediff( \'' . $date . '\', a.last_email) > '.ESHOP_SEND_EMAIL_DATE.'), 1, 0) as email_note')
			->from($this->mainTable . ' AS a ');
		
		$where = $this->_buildContentWhereArray();
		
		if (count($where))
		{
			$query->where($where);
		}
		
		$orderby = $this->_buildContentOrderBy();
		
		if ($orderby != '')
		{
			$query->order($orderby);
		}
		//echo $query->__toString();
		return $query;
	}
	
	/**
	 * Build an where clause array
	 *
	 * @return array
	 */
	public function _buildContentWhereArray()
	{
		$input = JFactory::getApplication()->input;
		$db = $this->getDbo();
		$state = $this->getState();
		$where = array();
		$orderStatusId = $input->getInt('order_status_id', 0);
		$paymentStatus = $input->getInt('payment_status', 0);
		if ($orderStatusId)
		{ 
			$where[] = 'a.order_status_id = ' . intval($orderStatusId);
		}

        if ($paymentStatus)
        {
            $pStatus = $paymentStatus == 2 ? 0 : $paymentStatus;
            if($pStatus == 3){
                $where[] = 'a.payment_status = 0';
                $date = JFactory::getDate()->toSql();
                $where[] = ' datediff( \'' . $date . '\', a.created_date) > 2';
            }else{
                $where[] = 'a.payment_status = ' . intval($pStatus);
            }

        }

		if ($state->search)
		{
			$search = $db->quote('%' . $db->escape($state->search, true) . '%', false);
			
			if (is_array($this->searchFields))
			{
				$whereOr = array();
				foreach ($this->searchFields as $titleField)
				{
					$whereOr[] = " LOWER($titleField) LIKE " . $search;
				}
				$where[] = ' (' . implode(' OR ', $whereOr) . ') ';
			}
			else
			{
				$where[] = 'LOWER(' . $this->searchFields . ') LIKE ' . $db->quote('%' . $db->escape($state->search, true) . '%', false);
			}
		}
		
		$shippingMethod = $input->getString('shipping_method');
		
		if ($shippingMethod != '')
		{
		    $where[] = 'a.id IN (SELECT order_id FROM #__eshop_ordertotals WHERE name = "shipping" AND title = ' . $db->quote($shippingMethod) .  ')';
		}
	
		return $where;
	}
}