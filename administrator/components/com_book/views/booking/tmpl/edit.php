<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Book
 * @author     vst <lvson1087@gmail.com>
 * @copyright  2019 @ Bizappco
 * @license    bản quyền mã nguồn mở GNU phiên bản 2
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\HTML\HTMLHelper;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Uri\Uri;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;


HTMLHelper::addIncludePath(JPATH_COMPONENT . '/helpers/html');
HTMLHelper::_('behavior.tooltip');
HTMLHelper::_('behavior.formvalidation');
HTMLHelper::_('formbehavior.chosen', 'select');
HTMLHelper::_('behavior.keepalive');

// Import CSS
$document = Factory::getDocument();
$document->addStyleSheet(Uri::root() . 'media/com_book/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {

	});

	Joomla.submitbutton = function (task) {
		if (task == 'booking.cancel') {
			Joomla.submitform(task, document.getElementById('booking-form'));
		}
		else {

			if (task != 'booking.cancel' && document.formvalidator.isValid(document.id('booking-form'))) {

				Joomla.submitform(task, document.getElementById('booking-form'));
			}
			else {
				alert('<?php echo $this->escape(Text::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_book&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="booking-form" class="form-validate form-horizontal">


	<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
	<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
	<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
	<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
	<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />
	<?php echo $this->form->renderField('created_by'); ?>
	<?php echo $this->form->renderField('modified_by'); ?>
	<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'lchhn')); ?>
	<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'lchhn', JText::_('COM_BOOK_TAB_LCHHN', true)); ?>
	<div class="row-fluid">
		<div class="span10 form-horizontal">
			<fieldset class="adminform">
				<legend><?php echo JText::_('COM_BOOK_FIELDSET_LCHHN'); ?></legend>

					<div class="control-group">
								<div class="control-label"><label id="jform_phone-lbl" for="jform_phone" class="required">
						Họ tên <span class="star">&nbsp;</span></label>
					</div>
							<div class="controls">
								<?php echo JFactory::getUser($this->item->created_by)->name; ?>
							</div>
					</div>
				<?php echo $this->form->renderField('phone'); ?>
				<?php echo $this->form->renderField('address'); ?>
				<?php echo $this->form->renderField('booking_date'); ?>
				<?php echo $this->form->renderField('hours'); ?>
				<?php echo $this->form->renderField('note'); ?>
				<?php echo $this->form->renderField('created_time'); ?>
				<?php echo $this->form->renderField('modified_time'); ?>
				<?php echo $this->form->renderField('status_id'); ?>
				<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
				<?php endif; ?>
			</fieldset>
		</div>
	</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>


	<?php echo JHtml::_('bootstrap.endTabSet'); ?>

	<input type="hidden" name="task" value=""/>
	<?php echo JHtml::_('form.token'); ?>

</form>
<style>
#toolbar-save-new,#toolbar-save-copy{ display:none;}
</style>
