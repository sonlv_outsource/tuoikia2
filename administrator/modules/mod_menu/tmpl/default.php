<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$doc       = JFactory::getDocument();
$direction = $doc->direction == 'rtl' ? 'pull-right' : '';
$class     = $enabled ? 'nav ' . $direction : 'nav disabled ' . $direction;

// Recurse through children of root node if they exist
$menuTree = $menu->getTree();
$root     = $menuTree->reset();

if ($root->hasChildren())
{
	echo '<ul id="menu" class="' . $class . '">' . "\n";

	// WARNING: Do not use direct 'include' or 'require' as it is important to isolate the scope for each call
	$menu->renderSubmenu(JModuleHelper::getLayoutPath('mod_menu', 'default_submenu'));
	//nganly


	$user   = JFactory::getUser();
	$groups = $user->get('groups');
	//print_r($groups);
	$hide7 = 0;
	$hide10 = 0;

	foreach ($groups as $group)
	{
			if($group == 7){
				$hide7 = 1;
			}
			if($group == 10){
				$hide10 = 1;
			}

	}
	$hide_group7_class = '';
	$hide_group10_class = '';


	if($hide7 == 1){
		$hide_group7_class = '';
	}else{
		//$hide_group7_class = '';
		if($hide10 == 1){
			$hide_group10_class = 'hidde-group10';
		}else{

		}
	}



	echo '<li class="'.$hide_group7_class.' "><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_eshop&view=orders">'.JText::_('MOD_MENU_PRODUCT_ORDER').'</a></li>';
	echo '<li class="'.$hide_group7_class.' "><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_eshop&view=productreports">Xuất Excel ĐH</a></li>';
	echo '<li class="'.$hide_group7_class.' '.$hide_group10_class.'"><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_eshop&view=stockreports">Thống kê Đơn hàng</a></li>';
	echo '<li class="'.$hide_group7_class.' '.$hide_group10_class.'"><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_users&view=users">Khách hàng & Quản trị</a></li>';
	echo '<li class="'.$hide_group7_class.' '.$hide_group10_class.'"><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_eshop&view=products">'.JText::_('MOD_MENU_PRODUCT_MANAGER').'</a></li>';
	echo '<li class="'.$hide_group7_class.' '.$hide_group10_class.'"><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_eshop&view=products&template=updateprice">Quản lý Giá</a></li>';
	echo '<li class="'.$hide_group7_class.' '.$hide_group10_class.'"><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_eshop&view=countries">Quản lý Tỉnh/TP</a></li>';
	echo '<li class="'.$hide_group7_class.' '.$hide_group10_class.'"><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_eshop&view=zones">Quản lý Quận</a></li>';
	echo '<li class="'.$hide_group7_class.' '.$hide_group10_class.'"><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_notification&view=notifications">Thông báo App</a></li>';
  echo '<li class="'.$hide_group7_class.' '.$hide_group10_class.'"><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_eshop&view=categories">'.JText::_('MOD_MENU_CAT_MANAGER').'</a></li>';
	echo '<li class="'.$hide_group7_class.' '.$hide_group10_class.'"><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_eshop&view=manufacturers">'.JText::_('MOD_MENU_MANU_MANAGER').'</a></li>';
	echo '<li class="'.$hide_group7_class.' '.$hide_group10_class.'"><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_eshop&view=options">'.JText::_('MOD_MENU_OPTION_MANAGER').'</a></li>';
	echo '<li class="'.$hide_group7_class.' '.$hide_group10_class.'"><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_eshop&view=weights">Đơn vị</a></li>';
	echo '<li class="'.$hide_group7_class.' '.$hide_group10_class.'"><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_banners">'.JText::_('MOD_MENU_PRODUCT_BANNER').'</a></li>';
	echo '<li class="'.$hide_group7_class.' '.$hide_group10_class.'"><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_eshop&view=coupons">Coupon</a></li>';
	echo '<li class="'.$hide_group7_class.' '.$hide_group10_class.'"><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_eshop&view=vouchers">Voucher</a></li>';
	echo '<li class="'.$hide_group7_class.' '.$hide_group10_class.'"><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_content">'.JText::_('MOD_MENU_PRODUCT_CONTENT').'</a></li>';
	echo "</ul>\n";

	echo '<ul id="nav-empty" class="dropdown-menu nav-empty hidden-phone"></ul>';

	if ($css = $menuTree->getCss())
	{
		$doc->addStyleDeclaration(implode("\n", $css));
	}
}
